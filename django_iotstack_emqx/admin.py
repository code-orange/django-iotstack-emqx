from django.apps import apps
from django.contrib import admin

for model in apps.get_app_config("django_iotstack_emqx").models.values():
    admin.site.register(model)
