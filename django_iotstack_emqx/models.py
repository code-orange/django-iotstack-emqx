from django.db import models


class MqttAcked(models.Model):
    clientid = models.CharField(max_length=200, blank=True, null=True)
    topic = models.CharField(max_length=200, blank=True, null=True)
    mid = models.IntegerField(blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "mqtt_acked"
        unique_together = (("clientid", "topic"),)


class MqttAcl(models.Model):
    allow = models.IntegerField(blank=True, null=True)
    ipaddr = models.CharField(max_length=60, blank=True, null=True)
    username = models.CharField(max_length=100, blank=True, null=True)
    clientid = models.CharField(max_length=100, blank=True, null=True)
    access = models.IntegerField()
    topic = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = "mqtt_acl"


class MqttClient(models.Model):
    clientid = models.CharField(unique=True, max_length=64, blank=True, null=True)
    state = models.CharField(max_length=3, blank=True, null=True)
    node = models.CharField(max_length=100, blank=True, null=True)
    online_at = models.DateTimeField(blank=True, null=True)
    offline_at = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "mqtt_client"


class MqttMsg(models.Model):
    msgid = models.CharField(max_length=100, blank=True, null=True)
    topic = models.CharField(max_length=1024)
    sender = models.CharField(max_length=1024, blank=True, null=True)
    node = models.CharField(max_length=60, blank=True, null=True)
    qos = models.IntegerField()
    retain = models.IntegerField(blank=True, null=True)
    payload = models.TextField(blank=True, null=True)
    arrived = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "mqtt_msg"


class MqttRetain(models.Model):
    topic = models.CharField(unique=True, max_length=200, blank=True, null=True)
    msgid = models.CharField(max_length=60, blank=True, null=True)
    sender = models.CharField(max_length=100, blank=True, null=True)
    node = models.CharField(max_length=100, blank=True, null=True)
    qos = models.IntegerField(blank=True, null=True)
    payload = models.TextField(blank=True, null=True)
    arrived = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "mqtt_retain"


class MqttSub(models.Model):
    clientid = models.CharField(max_length=64, blank=True, null=True)
    topic = models.CharField(max_length=255, blank=True, null=True)
    qos = models.IntegerField(blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "mqtt_sub"
        unique_together = (("clientid", "topic"),)


class MqttUser(models.Model):
    username = models.CharField(unique=True, max_length=100, blank=True, null=True)
    password = models.CharField(max_length=100, blank=True, null=True)
    salt = models.CharField(max_length=35, blank=True, null=True)
    is_superuser = models.IntegerField(blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "mqtt_user"
